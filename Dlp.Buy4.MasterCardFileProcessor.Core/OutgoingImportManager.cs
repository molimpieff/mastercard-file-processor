﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dlp.Buy4.MasterCardFileProcessor.Commons;
using Dlp.Buy4.MasterCardFileProcessor.Commons.DataObject;
using Dlp.Buy4.Framework.Messaging;
using Dlp.Buy4.Schemes.MasterCard.FileProcessing.Ipm;
using Dlp.Buy4.MasterCardFileProcessor.Core.Processor;
using Dlp.Buy4.MasterCardFileProcessor.DataContract;
using Dlp.Buy4.MasterCardFileProcessor.Repository.Clearing.Model;
using Dlp.Buy4.MasterCardFileProcessor.Repository.Clearing;


namespace Dlp.Buy4.MasterCardFileProcessor.Core
{
    /// <summary>
    /// manager responsible for using the processors for map the outgoingfile and store it on the database
    /// </summary>
    public class OutgoingImportManager
    {
        private ClearingFileRepository clearingFileRepository;
        private MasterCardFirstPresentmentRepository mfpRepository;
        private MasterCardFinancialAddendumRepository mfaRepository;
        private MasterCardFileHeaderRepository fileHeaderRepository;
        private MasterCardFileTrailerRepository fileTrailerRepository;
        private MasterCardFeeCollectionRepository mfcRepository;

        public OutgoingImportManager()
        {
            this.clearingFileRepository = new ClearingFileRepository();
            this.mfpRepository = new MasterCardFirstPresentmentRepository();
            this.mfaRepository = new MasterCardFinancialAddendumRepository();
            this.fileHeaderRepository = new MasterCardFileHeaderRepository();
            this.fileTrailerRepository = new MasterCardFileTrailerRepository();
            this.mfcRepository = new MasterCardFeeCollectionRepository();
        }

        /// <summary>
        /// Execute the outgoing import flow, to map the outgoing on the clearing database
        /// </summary>
        /// <param name="fileName">Name of the 'physical file'</param>
        /// <param name="filePath">path containing the file to be imported</param>
        public ServiceResponse Execute(string fileName, string filePath)
        {
            try
            {

                Log.WriteInformation("Initiated Import Outgoing");
                //instantiate the outgoingFile class
                OutgoingFile outgoingFile = new OutgoingFile();
                outgoingFile.FilePath = filePath;
                outgoingFile.FileName = fileName;

                //verify the fileName
                if(string.IsNullOrEmpty(fileName)||string.IsNullOrWhiteSpace(fileName))
                {
                    Log.WriteError("FileName invalid.");
                    return ServiceResponse.CreateError(new Exception("FileName invalid"));
                }

                //verify the filePath
                if(string.IsNullOrEmpty(filePath) || string.IsNullOrWhiteSpace(filePath))
                {
                    Log.WriteError("FilePath invalid");
                    return ServiceResponse.CreateError(new Exception("FilePath invalid."));
                }

                //verify the existence of the clearingFile on the database
                if(this.clearingFileRepository.ClearingFileExists(fileName) == true)
                {
                    Log.WriteError("Clearing File Already exists on the database.");
                    return ServiceResponse.CreateError(new Exception("Clearing File Already exists on the database."));
                }

                //lists used to store the collections in the OutgoingFile
                List<Presentment> presentmentList = new List<Presentment>();
                List<FinancialAddendum> financialAddendumList = new List<FinancialAddendum>();
                List<FeeCollection> feeCollectionList = new List<FeeCollection>();

                Log.WriteInformation("Started reading file");
                IEnumerable<IDataList> fileMessageCollection = MapFileHelper.GetFileMessageCollection(filePath + fileName);
                Log.WriteInformation("Finished Reading file");

                //map the messages on the fileMessageCollection
                Log.WriteInformation("Started Mapping messages");
                foreach (IDataList message in fileMessageCollection)
                {
                    //instantiate the processor with the message
                    OutgoingImportProcessor processor = new OutgoingImportProcessor(message);

                    //get the mti code to diferentiate the messages 
                    string mti = message["mti"];

                    if (message["mti"] == Convert.ToInt32(IpmFormatsEnum.FIRST_PRESENTMENT_MTI).ToString())
                    {
                        presentmentList.Add(processor.MapPresentment());

                    }
                    else if (message["mti"] == Convert.ToInt32(IpmFormatsEnum.MTI_1644).ToString())
                    {
                        if (message["24"] == Convert.ToInt32(IpmFormatsEnum.FINANCIAL_ADDENDUM_FUNCTION_CODE).ToString())
                        {
                            financialAddendumList.Add(processor.MapFinancialAddendum());
                        }
                        else if (message["24"] == Convert.ToInt32(IpmFormatsEnum.FILE_HEADER_FUNCTION_CODE).ToString())
                        {
                            outgoingFile.FileHeader = processor.MapFileHeader();
                        }
                        else if (message["24"] == Convert.ToInt32(IpmFormatsEnum.FILE_TRAILER_FUNCTION_CODE).ToString())
                        {
                            outgoingFile.FileTrailer = processor.MapFileTrailer();
                        }
                    }
                    else if (message["mti"] == Convert.ToInt32(IpmFormatsEnum.MTI_1740).ToString())
                    {
                        feeCollectionList.Add(processor.MapFeeCollection());
                    }
                }
                Log.WriteInformation("Finished Mapping messages");

                //set fileId to the outgoingfileObject
                outgoingFile.FileId = outgoingFile.FileTrailer.FileId;

                //set the lists with the messages mapped to the outgoingFile's collections
                outgoingFile.MasterCardPresentmentCollection = presentmentList;
                outgoingFile.FinancialAddendumCollection = financialAddendumList;
                outgoingFile.FeeCollectionCollection = feeCollectionList;

                int financialTransactionAmount = 0;
                foreach (Presentment presentment in outgoingFile.MasterCardPresentmentCollection)
                    financialTransactionAmount += int.Parse(presentment.AmountTransaction);

                if(outgoingFile.FeeCollectionCollection.Count > 0)
                    foreach(FeeCollection fee in outgoingFile.FeeCollectionCollection)
                        financialTransactionAmount += int.Parse(fee.AmountTransaction);

                outgoingFile.FinancialTransactionsAmount = financialTransactionAmount;

                //create the clearingFile on the DataBase.
                Log.WriteInformation("Creating ClearingFile on the Database.");
                int clearingFileId = clearingFileRepository.Create(outgoingFile);
                Log.WriteInformation("Created ClearingFile on the Database. ClearingFileId:"+clearingFileId);

                //create the fileHeader on the Database
                Log.WriteInformation("Creating FileHeader on the Database");
                fileHeaderRepository.Create(outgoingFile.FileHeader, clearingFileId);
                Log.WriteInformation("Created FileHeader on the Database");

                //create the FileTrailer on the Database
                Log.WriteInformation("Creating FileTrailer on the Database");
                fileTrailerRepository.Create(outgoingFile.FileTrailer, clearingFileId);
                Log.WriteInformation("Created FileTrailer on the Database");

                //insert the presentment's list on the database
                Log.WriteInformation("Inserting Presentment list on the Database");
                mfpRepository.BulkInsertPresentment(outgoingFile.MasterCardPresentmentCollection, clearingFileId);
                Log.WriteInformation("Inserted Presentment list on the Database");

                //insert the finnancialAddendum's list on the database 
                Log.WriteInformation("Inserting FinnancialAddendum list on the Database");
                mfaRepository.BulkInsertFinancialAddendum(outgoingFile.FinancialAddendumCollection, clearingFileId);
                Log.WriteInformation("Inserted FinnancialAddendum list on the Database");

                //verify if the file has FeeCollection messages
                if (outgoingFile.FeeCollectionCollection.Count > 0)
                {
                    //insert the feecollection's list on the database
                    Log.WriteInformation("Insering FeeCollection list on the Database");
                    this.mfcRepository.BulkInsertFeeCollection(outgoingFile.FeeCollectionCollection, clearingFileId);
                    Log.WriteInformation("Inserted FeeCollection list on the Database");
                }

                Log.WriteInformation("Outgoing import ended.");
                return ServiceResponse.CreateOk();
            }
            catch(Exception e)
            {
                Log.WriteError(e.Message);
                return ServiceResponse.CreateError(e);
            }
        }
    }
}
