﻿using Dlp.Buy4.MasterCardFileProcessor.Commons.DataObject;
using Dlp.Buy4.Schemes.MasterCard.FileProcessing.Ipm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dlp.Buy4.MasterCardFileProcessor.Commons;
using Dlp.Buy4.Framework.Messaging;

namespace Dlp.Buy4.MasterCardFileProcessor.Core.Processor
{
    public class OutgoingImportProcessor
    {

        private IpmTransaction ipmTransaction;

        /// <summary>
        /// default contructor must receive a IDatalist to map the message
        /// </summary>
        /// <param name="message">the file's message to be mapped by the processor</param>
        public OutgoingImportProcessor(IDataList message)
        {
            IpmTransaction ipm = new IpmTransaction();
            ipm.LoadData(message);
            this.ipmTransaction = ipm;
        }

        /// <summary>
        /// map the message as a Presentment object
        /// </summary>
        /// <returns>Presentment object mapped</returns>
        public Presentment MapPresentment()
        {
            Presentment masterCardPresentment = new Presentment();

            #region First/Second Presentment Mti=1240 FuncionCode=200, 205, 282
            masterCardPresentment.Mti = Convert.ToInt32(IpmFormatsEnum.FIRST_PRESENTMENT_MTI).ToString();

            masterCardPresentment.Pan = MapFileHelper.GetMaskedPan(this.ipmTransaction.PrimaryAccountNumber);
            masterCardPresentment.Bin = MapFileHelper.GetBin(this.ipmTransaction.PrimaryAccountNumber);

            masterCardPresentment.ProcessingCode = this.ipmTransaction.ProcessingCode;

            masterCardPresentment.AmountTransaction = this.ipmTransaction.AmountTransaction;
            masterCardPresentment.AmountReconciliation = this.ipmTransaction.AmountReconciliation;
            masterCardPresentment.AmountCardholderBilling = this.ipmTransaction.AmountCardholderBilling;

            masterCardPresentment.ConversionRateReconciliation = this.ipmTransaction.ConversionRateReconciliation;
            masterCardPresentment.ConversionRateCardholderBilling = this.ipmTransaction.ConversionRateCardholderBilling;

            masterCardPresentment.DateTimeLocalTransaction = this.ipmTransaction.DateAndTimeLocalTransaction;
            masterCardPresentment.ExpirationDate = this.ipmTransaction.DateExpiration;
            masterCardPresentment.PosData = this.ipmTransaction.PosDataCode;
            masterCardPresentment.CardSequenceNumber = this.ipmTransaction.CardSequenceNumber;
            masterCardPresentment.FunctionCode = this.ipmTransaction.FunctionCode;
            masterCardPresentment.Mcc = this.ipmTransaction.CardAcceptorBusinessCode;
            masterCardPresentment.AmountsOriginal = this.ipmTransaction.AmountsOriginal;
            masterCardPresentment.AcquirerReferenceData = this.ipmTransaction.AcquirerReferenceData;
            masterCardPresentment.AcquiringInstitutionIdCode = this.ipmTransaction.AcquiringInstitutionIdCode;
            masterCardPresentment.FowardInstitutionIdCode = this.ipmTransaction.ForwardingInstitutionIdCode;
            masterCardPresentment.RetrievalReferenceNumber = this.ipmTransaction.RetrievalReferenceNumber;
            masterCardPresentment.ApprovalCode = this.ipmTransaction.ApprovalCode;

            masterCardPresentment.CardAcceptorId = this.ipmTransaction.CardAcceptorIdCode;
            masterCardPresentment.CardAcceptorNameLocation = this.ipmTransaction.CardAcceptorNameLocation;

            masterCardPresentment.CurrencyCodeTransaction = this.ipmTransaction.CurrencyCodeTransaction;
            masterCardPresentment.CurrencyCodeReconciliation = this.ipmTransaction.CurrencyCodeReconciliation;
            masterCardPresentment.CurrencyCodeCardholderBilling = this.ipmTransaction.CurrencyCodeCardholderBilling;

            masterCardPresentment.AmountsAdditional = this.ipmTransaction.AmountsAdditional;
            masterCardPresentment.TransactionLifecycleId = this.ipmTransaction.TransactionLifeCycleId;
            masterCardPresentment.MessageNumber = int.Parse(this.ipmTransaction.MessageNumber);
            masterCardPresentment.DataRecord = this.ipmTransaction.DataRecord;
            masterCardPresentment.TransactionDestinationInsutitutionId = this.ipmTransaction.TransactionDestinationInstitutionIdCode;
            masterCardPresentment.TransactionOriginatorInstitutionIdCode = this.ipmTransaction.TransactionOriginatorInstitutionIdCode;
            masterCardPresentment.CardIssuerReferenceData = this.ipmTransaction.CardIssuerReferenceData;

            masterCardPresentment.ReceivingInstitutionId = this.ipmTransaction.ReceivingInstitutionIdCode;
            masterCardPresentment.AmountCurrencyConversionAssessment = this.ipmTransaction.AmountCurrencyConversionAssessment;
            
            // Pds Data
            masterCardPresentment.TerminalType = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0023");
            masterCardPresentment.MessageReversalIndicator = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0025");
            masterCardPresentment.FileReversalIndicator = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0026");
            masterCardPresentment.Eci = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0052");
            masterCardPresentment.CurrencyExponents = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0148");
            masterCardPresentment.BusinessActivity = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0158");
            masterCardPresentment.SettlementIndicator = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0165");
            masterCardPresentment.BrazilMerchantTaxId = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0220");

            masterCardPresentment.PdsData = MapFileHelper.SerializePds(this.ipmTransaction.Pds);
            
            return masterCardPresentment;
            
            #endregion

        }
    
        /// <summary>
        /// map the message as a FinancialAddendum object
        /// </summary>
        /// <returns>FinancialAddendum object mapped</returns>
        public FinancialAddendum MapFinancialAddendum ()
        {
            FinancialAddendum financialAddendum = new FinancialAddendum();

            financialAddendum.Mti = Convert.ToInt32(IpmFormatsEnum.MTI_1644).ToString();
            financialAddendum.FunctionCode = this.ipmTransaction.FunctionCode;

            financialAddendum.AcquiringInstitutionIdCode = this.ipmTransaction.AcquiringInstitutionIdCode;
            financialAddendum.FowardInstutionIdCode = this.ipmTransaction.ForwardingInstitutionIdCode;
            financialAddendum.MessageNumber = int.Parse(this.ipmTransaction.MessageNumber);
            financialAddendum.TransactionDestinationInsutitutionId = this.ipmTransaction.TransactionDestinationInstitutionIdCode;
            financialAddendum.TransactionOriginatorInstitutionId = this.ipmTransaction.TransactionOriginatorInstitutionIdCode;
            financialAddendum.ReceivingInstitutionId = this.ipmTransaction.ReceivingInstitutionIdCode;

            // PDS data.
            // financialAddendum.PdsData = SerializeDataList(pdsMsg);

            string TransactionDescription = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0501");
            financialAddendum.TransactionDescription = TransactionDescription;
            if (!string.IsNullOrEmpty(TransactionDescription))
            {
                financialAddendum.TransactionDescriptionUsageCode = TransactionDescription.Substring(0, 2);
                financialAddendum.TransactionDescriptionIndustryRecordNumber = TransactionDescription.Substring(2, 3);
                financialAddendum.TransactionDescriptionOccurrenceIndicator = TransactionDescription.Substring(5, 3);
                financialAddendum.TransactionDescriptionAssociatedFirstPresentment = TransactionDescription.Substring(8, 8);
            }

            #region FreeFormDescription - Installment data
            string FreeFormDescription = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0663");
            financialAddendum.FreeFormDescription = FreeFormDescription;
            if (string.IsNullOrEmpty(FreeFormDescription) == false && MapFileHelper.IsInstallmentFreeFormDescription(FreeFormDescription))
            {
                if (FreeFormDescription.Length >= 3)
                    financialAddendum.TypeOfInstallmentPayment = FreeFormDescription.Substring(0, 3);

                if (FreeFormDescription.Length >= 15)
                    financialAddendum.SalesAmount = FreeFormDescription.Substring(3, 12);

                if (FreeFormDescription.Length >= 17)
                    financialAddendum.NumberOfInstallments = FreeFormDescription.Substring(15, 2);

                if (FreeFormDescription.Length >= 19)
                    financialAddendum.InstallmentPaymentNumber = FreeFormDescription.Substring(17, 2);

                if (FreeFormDescription.Length >= 31)
                    financialAddendum.InstallmentPaymentAmount = FreeFormDescription.Substring(19, 12);

                if (FreeFormDescription.Length >= 43)
                    financialAddendum.AirportFeeAmount = FreeFormDescription.Substring(31, 12);

                if (FreeFormDescription.Length >= 66)
                    financialAddendum.DownPaymentAmount = FreeFormDescription.Substring(54, 12);
            }
            #endregion

            return financialAddendum;

        }

        /// <summary>
        /// map the message as a FeeCollection object
        /// </summary>
        /// <returns>FeeCollection object mapped</returns>
        public FeeCollection MapFeeCollection ()
        {
            FeeCollection feeCollection = new FeeCollection();

            feeCollection.Mti = Convert.ToInt32(IpmFormatsEnum.MTI_1740).ToString();

            //Checking all data elements
            feeCollection.Pan = MapFileHelper.GetMaskedPan(this.ipmTransaction.PrimaryAccountNumber);
            feeCollection.ProcessingCode = this.ipmTransaction.ProcessingCode;
            feeCollection.AmountTransaction = this.ipmTransaction.AmountTransaction;
            feeCollection.AmountReconciliation = this.ipmTransaction.AmountReconciliation;
            feeCollection.ConversionRateReconciliation = this.ipmTransaction.ConversionRateReconciliation;
            feeCollection.CardSequenceNumber = this.ipmTransaction.CardSequenceNumber;
            feeCollection.FunctionCode = this.ipmTransaction.FunctionCode;
            feeCollection.MessageReasonCode = this.ipmTransaction.MessageReasonCode;
            feeCollection.MCC = this.ipmTransaction.CardAcceptorBusinessCode;
            feeCollection.AmountsOriginal = this.ipmTransaction.AmountsOriginal;
            feeCollection.AcquirerReferenceData = this.ipmTransaction.AcquirerReferenceData;
            feeCollection.AcquiringInstitutionIdentificationCode = this.ipmTransaction.AcquiringInstitutionIdCode;
            feeCollection.ForwardingInstitutionIdCode = this.ipmTransaction.ForwardingInstitutionIdCode;
            feeCollection.ApprovalCode = this.ipmTransaction.ApprovalCode;
            feeCollection.CardAcceptorTerminalId = this.ipmTransaction.CardAcceptorTerminalId;
            feeCollection.CardAcceptorIdCode = this.ipmTransaction.CardAcceptorIdCode;
            feeCollection.CardAcceptorNameLocation = this.ipmTransaction.CardAcceptorNameLocation;
            feeCollection.CurrencyCodeTransaction = this.ipmTransaction.CurrencyCodeTransaction;
            feeCollection.CurrencyCodeReconciliation = this.ipmTransaction.CurrencyCodeReconciliation;
            feeCollection.TransactionLifeCycleId = this.ipmTransaction.TransactionLifeCycleId;
            feeCollection.MessageNumber = int.Parse(this.ipmTransaction.MessageNumber);
            feeCollection.DataRecord = this.ipmTransaction.DataRecord;
            feeCollection.DateAction = this.ipmTransaction.DateAction;
            feeCollection.TransactionDestinationInstitutionIdCode = this.ipmTransaction.TransactionDestinationInstitutionIdCode;
            feeCollection.TransactionOriginatorInstitutionIdCode = this.ipmTransaction.TransactionOriginatorInstitutionIdCode;
            feeCollection.ReceivingInstitutionIdCode = this.ipmTransaction.ReceivingInstitutionIdCode;

            //Checking all private data subelements
            feeCollection.GCMSProductIdentifier = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0002");
            feeCollection.LicensedProductIdentifier = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0003");
            feeCollection.MessageReversalIndicator = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0025");
            feeCollection.AmountTax = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0080");
            feeCollection.FeeCollectionControlNumber = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0137");
            feeCollection.CurrencyExponents = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0148");
            feeCollection.CurrencyCodesAmountsOriginal = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0149");
            feeCollection.BusinessActivity = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0158");
            feeCollection.SettlementData = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0159");
            feeCollection.SettlementIndicator = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0165");
            feeCollection.CardAcceptorURL = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0175");
            feeCollection.OriginatingMessageFormat = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0191");
            feeCollection.DocumentationIndicator = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0262");
            feeCollection.InitialPresentmentFeeCollectionData = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0265");
            feeCollection.FirstChargebackFeeCollectionReturnData = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0266");
            feeCollection.SecondPresentmentFeeCollectionResubmissionData = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0267");
            feeCollection.MemberReconciliationIndicatorOne = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0375");
            feeCollection.TestCaseTraceabilityIdentifiers = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0799");
            feeCollection.MemberToMemberProprietaryData = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "1000-1099");

            return feeCollection;
        }

        /// <summary>
        /// map the message as a FileHeader object
        /// </summary>
        /// <returns>OutgoingFileHeader object mapped</returns>
        public OutgoingFileHeader MapFileHeader ()
        {
            OutgoingFileHeader fileHeader = new OutgoingFileHeader();

            fileHeader.Mti = Convert.ToInt32(IpmFormatsEnum.MTI_1644).ToString();
            fileHeader.FunctionCode = this.ipmTransaction.FunctionCode;
            fileHeader.MessageNumber = int.Parse(this.ipmTransaction.MessageNumber);

            fileHeader.FileReversalIndicator = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0026");
            fileHeader.FileId = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0105");
            fileHeader.TransmissionId = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0110");
            fileHeader.ProcessingMode = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0122");
            fileHeader.OriginatingMessageFormat = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0191");

            return fileHeader;
        }

        /// <summary>
        /// map the message as a FileTrailer object
        /// </summary>
        /// <returns>OutgoingFileTrailer object mapped</returns>
        public OutgoingFileTrailer MapFileTrailer()
        {
            OutgoingFileTrailer fileTrailer = new OutgoingFileTrailer();

            fileTrailer.Mti = Convert.ToInt32(IpmFormatsEnum.MTI_1644).ToString();
            fileTrailer.FunctionCode = this.ipmTransaction.FunctionCode;
            fileTrailer.MessageNumber = int.Parse(this.ipmTransaction.MessageNumber);

            fileTrailer.FileId = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0105");
            fileTrailer.OriginatingMessageFormat = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0191");
            fileTrailer.FileAmountChecksum = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0301");
            fileTrailer.FileMessageCounts = MapFileHelper.GetDefaultPds(this.ipmTransaction.Pds, "0306");

            return fileTrailer;
        }
    }
}
