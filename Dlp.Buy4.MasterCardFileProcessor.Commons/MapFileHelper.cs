﻿using Dlp.Buy4.Framework.Messaging;
using Dlp.Buy4.Framework.Utils;
using Dlp.Buy4.Schemes.Iso8583;
using Dlp.Buy4.Schemes.MasterCard.FileProcessing.Ipm;
using Dlp.Buy4.Schemes.MasterCard.FileProcessing.VariableBlock;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dlp.Buy4.MasterCardFileProcessor.Commons
{
    public static class MapFileHelper
    {

        #region mapping helper methods
        /// <summary>
        /// Returns data[key], defaults to null.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetDefault(IDataList data, string key)
        {
            return GetDefault(data, key, null);
        }

        /// <summary>
        /// Return data[key], defaults to 'defaultValue'.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static string GetDefault(IDataList data, string key, string defaultValue)
        {
            if (data.ContainsKey(key))
            {
                return data[key];
            }
            return defaultValue;
        }

        public static string GetDefaultPds(IpmPrivateDataSubelementList data, string key)
        {
            IpmPrivateDataSubelement pds;
            data.Items.TryGetValue(key, out pds);

            if (pds != null)
            {
                ICollection<string> pdsValues = pds.Values;
                StringBuilder sb = new StringBuilder();
                foreach (string pdsValue in pdsValues)
                {
                    sb.Append(pdsValue);
                }

                return sb.ToString();
            }
            else
            {
                return null;
            }
        }

        public static string SerializePds(IpmPrivateDataSubelementList pds)
        {
            if (pds != null)
            {
                StringBuilder sb = new StringBuilder();
                foreach (KeyValuePair<string, IpmPrivateDataSubelement> item in pds.Items)
                {
                    //sb.Append(string.Format("{0}={1};", item.Key, item.Value.get));
                    ICollection<string> pdsValueCollection = item.Value.Values;
                    StringBuilder pdsBuilder = new StringBuilder();

                    foreach (string pdsValue in pdsValueCollection)
                    {
                        pdsBuilder.Append(pdsValue);
                    }

                    sb.Append(string.Format("{0}={1};", item.Key, pdsBuilder.ToString()));
                }

                return sb.ToString();
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Obfuscates the digits 6-12 from the given string
        /// </summary>
        /// <param name="pan"></param>
        /// <returns></returns>
        public static string GetMaskedPan(string pan)
        {
            if (pan.Length == 16)
                return pan.Substring(0, 6) + "******" + pan.Substring(12, 4);
            else
                return pan.Substring(0, 6) + "*********" + pan.Substring(15, 4);
        }

        /// <summary>
        /// Returns the first 6 digits from the string
        /// </summary>
        /// <param name="pan"></param>
        /// <returns></returns>
        public static string GetBin(string pan)
        {
            return pan.Substring(0, 6);
        }

        /// <summary>
        /// Serializes the an IDataList using the format: "key=value;key2=value; ..."
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string SerializeDataList(IDataList data)
        {
            StringBuilder sb = new StringBuilder();
            foreach (KeyValuePair<string, string> item in data)
            {
                sb.Append(string.Format("{0}={1};", item.Key, item.Value));
            }
            return sb.ToString();
        }

        /// <summary>
        /// Checks if FreeFormDescription begins with a valid Type of Installment Payment.
        /// </summary>
        /// <param name="desc"></param>
        /// <returns></returns>
        public static bool IsInstallmentFreeFormDescription(string desc)
        {
            if (desc.StartsWith("403"))
                return true;
            if (desc.StartsWith("407"))
                return true;
            if (desc.StartsWith("488"))
                return true;
            if (desc.StartsWith("410"))
                return true;

            return false;
        }
        #endregion


        #region file read methods
        /// <summary>
        /// Gets all the file messages
        /// </summary>
        /// <param name="fileName">FileName + it's path</param>
        /// <returns>Collection with all the file's messages</returns>
        public static IEnumerable<IDataList> GetFileMessageCollection(string fileName)
        {
            MasterCardBlock1014RecordReader mastercardRecordReader = new MasterCardBlock1014RecordReader();
            byte[] buffer;
            List<byte> byteCollection = new List<byte>();
            FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            List<IDataList> messageCollection = new List<IDataList>();

            mastercardRecordReader.BeginReadJob(fileStream);
            buffer = mastercardRecordReader.ReadNextRecord();
            while (buffer != null)
            {
                //IDataList datalist = new MasterCardIpmAsciiMsg();
                IDataList datalist = new Iso8583Msg();

                string data = HexUtils.FromBytes(buffer);
                //UnpackResult<IDataList> packResult = new MasterCardIpmAsciiSerializer().Unpack(data, datalist);
                UnpackResult<IDataList> packResult = new IpmAsciiSerializer().Unpack(data, datalist);

                messageCollection.Add(packResult.Value);
                buffer = mastercardRecordReader.ReadNextRecord();
            }
            mastercardRecordReader.EndReadJob();

            return messageCollection;
        }
        #endregion
    }
}
