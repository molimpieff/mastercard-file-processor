﻿using Dlp.Buy4.Log;
using Dlp.Buy4.Log.Listeners;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dlp.Buy4.MasterCardFileProcessor.Commons
{
    public sealed class Log
    {
        private static LogWriter logWriter = new LogWriter(new EnterpriseLibraryLogListener());

        public static void WriteUndefined(object objectToLog) { logWriter.Write(objectToLog, LogCategory.Undefined); }

        public static void WriteError(object objectToLog) { logWriter.Write(objectToLog, LogCategory.Error); }

        public static void WriteWarning(object objectToLog) { logWriter.Write(objectToLog, LogCategory.Warning); }

        public static void WriteInformation(object objectToLog) { logWriter.Write(objectToLog, LogCategory.Information); }
    }
}
