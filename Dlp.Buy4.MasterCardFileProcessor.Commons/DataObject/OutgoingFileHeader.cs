﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dlp.Buy4.MasterCardFileProcessor.Commons.DataObject
{
    public class OutgoingFileHeader
    {
        
        public String FileId { get; set; }

        public String TransmissionId { get; set; }

        public String FunctionCode { get; set; }

        public String OriginatingMessageFormat { get; set; }

        public Int32 MessageNumber { get; set; }

        public String ProcessingMode { get; set; }

        public String FileReversalIndicator { get; set; }

        public String Mti { get; set; }

    }
}
