﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dlp.Buy4.Schemes.MasterCard.FileProcessing.Ipm;
using Dlp.Buy4.Schemes.MasterCard.FileProcessing.VariableBlock;
using Dlp.Buy4.Framework.Messaging;

namespace Dlp.Buy4.MasterCardFileProcessor.Commons.DataObject
{
    public class OutgoingFile
    {
        public string FileName { get; set; }

        public string FilePath { get; set; }

        public ICollection<Presentment> MasterCardPresentmentCollection { get; set; }

        public ICollection<FinancialAddendum> FinancialAddendumCollection { get; set; }

        public OutgoingFileHeader FileHeader { get; set; }

        public OutgoingFileTrailer FileTrailer { get; set; }

        public ICollection<FeeCollection> FeeCollectionCollection { get; set; }

        public int FinancialTransactionsCount {
            get 
            {
                if (this.FeeCollectionCollection.Count > 0)
                    return this.FeeCollectionCollection.Count;
                else
                    return MasterCardPresentmentCollection.Count; 
            }  
        }

        public long FinancialTransactionsAmount { get; set; }

        public DateTime ProcessingDate { get; set; }

        public string FileId { get; set; }
    }
}
