﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dlp.Buy4.MasterCardFileProcessor.Commons.DataObject
{
    public sealed class ErrorReport
    {
        #region Properties

        /// <summary>
        /// Nome da classe que reportou erro
        /// </summary>
        public string ClassName { get; set; }

        /// <summary>
        /// Mensagem do erro
        /// </summary>
        public string Message { get; set; }

        #endregion


        #region Constructors

        /// <summary>
        /// Construtor padrão
        /// </summary>
        public ErrorReport() { }

        /// <summary>
        /// Construtor com carga
        /// </summary>
        /// <param name="className">Nome da classe que reportou o erro</param>
        /// <param name="Message">Mensagem do erro</param>
        public ErrorReport(string className, string Message)
        {
            this.ClassName = className;
            this.Message = Message;
        }

        #endregion


        #region Overrided Methods

        public override string ToString()
        {
            return string.Format("({0}) Error: {1}", this.ClassName, this.Message);
        }

        #endregion
    }
}
