﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dlp.Buy4.MasterCardFileProcessor.Commons.DataObject
{
    public sealed class FeeCollection
    {
        public String ReceivingInstitutionIdCode { get; set; }

        public String TransactionOriginatorInstitutionIdCode { get; set; }

        public String TransactionDestinationInstitutionIdCode { get; set; }

        public String DateAction { get; set; }

        public String DataRecord { get; set; }

        public Int32 MessageNumber { get; set; }

        public String TransactionLifeCycleId { get; set; }

        public String CurrencyCodeReconciliation { get; set; }

        public String CurrencyCodeTransaction { get; set; }

        public String MemberToMemberProprietaryData { get; set; }

        public String TestCaseTraceabilityIdentifiers { get; set; }

        public String MemberReconciliationIndicatorOne { get; set; }

        public String SecondPresentmentFeeCollectionResubmissionData { get; set; }

        public String FirstChargebackFeeCollectionReturnData { get; set; }

        public String InitialPresentmentFeeCollectionData { get; set; }

        public String DocumentationIndicator { get; set; }

        public String OriginatingMessageFormat { get; set; }

        public String CardAcceptorURL { get; set; }

        public String SettlementIndicator { get; set; }

        public String SettlementData { get; set; }

        public String BusinessActivity { get; set; }

        public String CurrencyCodesAmountsOriginal { get; set; }

        public String CurrencyExponents { get; set; }

        public String FeeCollectionControlNumber { get; set; }

        public String AmountTax { get; set; }

        public String MessageReversalIndicator { get; set; }

        public String LicensedProductIdentifier { get; set; }

        public String GCMSProductIdentifier { get; set; }

        public String CardAcceptorNameLocation { get; set; }

        public String CardAcceptorIdCode { get; set; }

        public String CardAcceptorTerminalId { get; set; }

        public String ApprovalCode { get; set; }

        public String ForwardingInstitutionIdCode { get; set; }

        public String AcquiringInstitutionIdentificationCode { get; set; }

        public String AcquirerReferenceData { get; set; }

        public String AmountsOriginal { get; set; }

        public String MCC { get; set; }

        public String MessageReasonCode { get; set; }

        public String FunctionCode { get; set; }

        public String CardSequenceNumber { get; set; }

        public String ConversionRateReconciliation { get; set; }

        public String AmountReconciliation { get; set; }

        public String AmountTransaction { get; set; }

        public String ProcessingCode { get; set; }

        public String Pan { get; set; }

        public String Mti { get; set; }
    }
}
