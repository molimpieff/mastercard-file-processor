﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dlp.Buy4.MasterCardFileProcessor.Commons.DataObject
{
    public sealed class Presentment
    {
        public String Mti { get; set; }

        public String Bin { get; set; }

        public String PdsData { get; set; }

        public String PosData { get; set; }

        public String CardAcceptorTerminalId { get; set; }

        public String CardIssuerReferenceData { get; set; }

        public String DataRecord { get; set; }

        public String CardSequenceNumber { get; set; }

        public String CardAcceptorNameLocation { get; set; }

        public String CardAcceptorId { get; set; }

        public String ApprovalCode { get; set; }

        public String ServiceCode { get; set; }

        public String TransactionLifecycleId { get; set; }

        public String CurrencyCodeTransaction { get; set; }

        public String CurrencyCodeCardholderBilling { get; set; }

        public String AmountsAdditional { get; set; }

        public String CurrencyCodeReconciliation { get; set; }

        public String FowardInstitutionIdCode { get; set; }

        public String FileReversalIndicator { get; set; }

        public String ReceivingInstitutionId { get; set; }

        public String RetrievalReferenceNumber { get; set; }

        public String AcquiringInstitutionIdCode { get; set; }

        public String AmountCurrencyConversionAssessment { get; set; }

        public String AcquirerReferenceData { get; set; }

        public String MessageReversalIndicator { get; set; }

        public String Mcc { get; set; }

        public String ExpirationDate { get; set; }

        public String DateTimeLocalTransaction { get; set; }

        public String TransactionOriginatorInstitutionId { get; set; }

        public String TransactionDestinationInsutitutionId { get; set; }

        public String TransactionOriginatorInstitutionIdCode { get; set; }

        public String AmountsOriginal { get; set; }

        public String TerminalType { get; set; }

        public String AmountCardholderBilling { get; set; }

        public String AmountReconciliation { get; set; }

        public String AmountTransaction { get; set; }

        public String MessageReasonCode { get; set; }

        public String CurrencyExponents { get; set; }

        public Int32 MessageNumber { get; set; }

        public String ProcessingCode { get; set; }

        public String Pan { get; set; }

        public String SettlementIndicator { get; set; }

        public String BusinessActivity { get; set; }

        public String ConversionRateCardholderBilling { get; set; }

        public String ConversionRateReconciliation { get; set; }

        public String Eci { get; set; }

        public String FunctionCode { get; set; }

        public String BrazilMerchantTaxId { get; set; }
    }
}
