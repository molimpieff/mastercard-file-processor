﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dlp.Buy4.MasterCardFileProcessor.Commons.DataObject
{
    public class OutgoingFileTrailer
    {
        public Int32 MessageNumber { get; set; }

        public String FunctionCode { get; set; }

        public String FileAmountChecksum { get; set; }

        public String FileMessageCounts { get; set; }

        public String OriginatingMessageFormat { get; set; }

        public String FileId { get; set; }

        public String Mti { get; set; }
    }
}
