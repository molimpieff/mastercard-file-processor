﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dlp.Buy4.MasterCardFileProcessor.Commons.DataObject
{
    public sealed class FinancialAddendum
    {
        public Int32 MessageNumber { get; set; }

        public String NumberOfInstallments { get; set; }

        public String FowardInstutionIdCode { get; set; }

        public String AcquiringInstitutionIdCode { get; set; }

        public String ReceivingInstitutionId { get; set; }

        public String TransactionDestinationInsutitutionId { get; set; }

        public String SalesAmount { get; set; }

        public String TypeOfInstallmentPayment { get; set; }

        public String PdsData { get; set; }

        public String AirportFeeAmount { get; set; }

        public String FreeFormDescription { get; set; }

        public String DownPaymentAmount { get; set; }

        public String TransactionDescriptionAssociatedFirstPresentment { get; set; }

        public String TransactionDescriptionIndustryRecordNumber { get; set; }

        public String InstallmentPaymentAmount { get; set; }

        public String TransactionDescription { get; set; }

        public String InstallmentPaymentNumber { get; set; }

        public String TransactionDescriptionUsageCode { get; set; }

        public String TransactionDescriptionOccurrenceIndicator { get; set; }

        public String TransactionOriginatorInstitutionId { get; set; }

        public String FunctionCode { get; set; }

        public String Mti { get; set; }
    }
}
