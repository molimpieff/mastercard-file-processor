﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;


namespace Dlp.Buy4.MasterCardFileProcessor.DataContract
{
    public class ServiceException : Exception
    {
        private HttpStatusCode httpStatusCode;
        /// <summary>
        /// HttpStatusCode passed to WebOperationContext Response HttpStatus.
        /// </summary>
        public HttpStatusCode HttpStatusCode { get { return httpStatusCode; } }

        private ICollection<string> additionalMessages;
        public ICollection<string> AdditionalMessages { get { return additionalMessages; } }

        /// <summary>
        /// Initializes a new instance of the ServiceInfrastructure.Exceptions.ServiceException class with a specified
        /// error message and additional messages.
        /// Set the WebOperationContext Response HttpStatus to BadRequest(400).
        /// </summary>
        /// <param name="message"></param>
        public ServiceException(string serviceExceptionMessage, params string[] additionalMessages)
            : this(serviceExceptionMessage, HttpStatusCode.BadRequest, additionalMessages)
        { }

        /// <summary>
        /// Initializes a new instance of the ServiceInfrastructure.Exceptions.ServiceException class with a specified
        /// error message and additional messages.
        /// Set the WebOperationContext Response HttpStatus to BadRequest(400).
        /// </summary>
        /// <param name="message"></param>
        public ServiceException(string serviceExceptionMessage, Exception innerException, params string[] additionalMessages)
            : this(serviceExceptionMessage, HttpStatusCode.BadRequest, innerException, additionalMessages)
        { }

        public ServiceException(ServiceResponse serviceResponse)
            : this("See the AdditionalMessages property for more information.", (HttpStatusCode)serviceResponse.StatusCode, GetServiceResponseMessages(serviceResponse))
        { }

        public ServiceException(ServiceResponse serviceResponse, Exception innerException)
            : this("See the AdditionalMessages property for more information.", (HttpStatusCode)serviceResponse.StatusCode, innerException, GetServiceResponseMessages(serviceResponse))
        { }

        /// <summary>
        /// Initializes a new instance of the ServiceInfrastructure.Exceptions.ServiceException class with a specified
        /// error message and additional messages.
        /// Set the WebOperationContext Response HttpStatus to received value.
        /// </summary>
        /// <param name="serviceExceptionMessage"></param>
        /// <param name="httpStatusCode"></param>
        /// <param name="additionalMessages"></param>
        public ServiceException(string serviceExceptionMessage, HttpStatusCode httpStatusCode, params string[] additionalMessages)
            : this(serviceExceptionMessage, httpStatusCode, null, additionalMessages)
        { }

        /// <summary>
        /// Initializes a new instance of the ServiceInfrastructure.Exceptions.ServiceException class with a specified
        /// error message and additional messages.
        /// Set the WebOperationContext Response HttpStatus to received value.
        /// </summary>
        /// <param name="serviceExceptionMessage"></param>
        /// <param name="httpStatusCode"></param>
        /// <param name="additionalMessages"></param>
        public ServiceException(string serviceExceptionMessage, HttpStatusCode httpStatusCode, Exception innerException, params string[] additionalMessages)
            : base(serviceExceptionMessage, innerException)
        {
            this.httpStatusCode = httpStatusCode;
            SetAdditonalMessages(additionalMessages);
            HttpContextWrapper.ChangeResponseHttpStatus(httpStatusCode);
        }
        private static string[] GetServiceResponseMessages(ServiceResponse serviceResponse)
        {
            return serviceResponse.AdditionalMessages.Select(sm => sm.Message).ToArray();
        }

        private void SetAdditonalMessages(string[] additionalMessages)
        {
            this.additionalMessages = new List<string>(additionalMessages.ToList());
        }
    }
}
