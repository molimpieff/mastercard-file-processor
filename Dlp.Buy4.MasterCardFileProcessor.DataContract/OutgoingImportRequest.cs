﻿using System.Runtime.Serialization;

namespace Dlp.Buy4.MasterCardFileProcessor.DataContract
{
    [DataContract]
    public class OutgoingImportRequest
    {
        string fileName;
        string filePath;

        [DataMember]
        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }

        [DataMember]
        public string FilePath
        {
            get { return filePath; }
            set { filePath = value; }
        }
    }
}
