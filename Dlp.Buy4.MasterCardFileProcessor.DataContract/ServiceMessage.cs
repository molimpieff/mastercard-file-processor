﻿using System.Runtime.Serialization;

namespace Dlp.Buy4.MasterCardFileProcessor.DataContract
{
    [DataContract(Name = "ServiceMessage")]
    public class ServiceMessage
    {
        [DataMember]
        public string Message { get; set; }
    }
}
