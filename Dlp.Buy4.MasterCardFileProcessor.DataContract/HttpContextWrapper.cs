﻿using System.Net;
using System.ServiceModel.Web;

namespace Dlp.Buy4.MasterCardFileProcessor.DataContract
{
    public class HttpContextWrapper
    {
        /// <summary>
        /// Change the response http status if WebOperationContext is valid.
        /// </summary>
        /// <param name="httpStatusCode"></param>
        public static void ChangeResponseHttpStatus(HttpStatusCode httpStatusCode)
        {
            if (WebOperationContext.Current != null)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = httpStatusCode;
            }
        }
    }
}
