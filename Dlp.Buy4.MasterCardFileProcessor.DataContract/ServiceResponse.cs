﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Dlp.Buy4.MasterCardFileProcessor.DataContract
{
    /// <summary>
    /// The response contract to an service request.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [DataContract(Name = "ServiceResponseOf{0}")]
    public class ServiceResponse<T> : ServiceResponse
    {
        /// <summary>
        /// Response content.
        /// </summary>
        [DataMember]
        [XmlElement]
        public T Data { get; set; }

        /// <summary>
        /// Create a ServiceResponse with failure state, and a default internal error message on ValidationErrors property,
        /// logs the error and set the Response HttpStatus to 500(Internal Server Error).
        /// </summary>
        /// <param name="unexpectedException">Unexpected Exception</param>
        /// <returns>ServiceResponse of generics with failure state</returns>
        public static ServiceResponse<T> CreateError(Exception unexpectedException, bool logError = true)
        {
            ServiceResponse baseResponse = ServiceResponse.CreateError(unexpectedException, logError);

            return new ServiceResponse<T>
            {
                Success = baseResponse.Success,
                AdditionalMessages = baseResponse.AdditionalMessages,
                StatusCode = baseResponse.StatusCode,
                Data = default(T)
            };
        }

        /// <summary>
        /// Create a ServiceResponse<T> with failure state, and add the exception message to ValidationErrors property, and logs the error.
        /// </summary>
        /// <param name="serviceException">throwed ServiceException</param>
        /// <returns>ServiceResponse<T> with failure state</returns>
        public static ServiceResponse<T> CreateError(ServiceException serviceException, bool logError = true)
        {
            ServiceResponse baseResponse = ServiceResponse.CreateError(serviceException, logError);
            return new ServiceResponse<T>
            {
                Success = baseResponse.Success,
                AdditionalMessages = baseResponse.AdditionalMessages,
                StatusCode = baseResponse.StatusCode,
                Data = default(T)
            };
        }

        /// <summary>
        /// Retorna um ServiceResponse de falha, mas com dados a serem retornados
        /// </summary>
        /// <param name="statusCode">HttpStatusCode de falha 4XX ou 5XX</param>
        /// <param name="additionalMessages">mensagens a serem enviadas na resposta</param>
        /// <param name="data">dados a serrem enviados no corpo do response</param>
        /// <exception cref="System.ArgumentException"></exception>
        public static ServiceResponse<T> CreateError(HttpStatusCode httpStatusCode, ICollection<ServiceMessage> additionalMessages, T data)
        {
            int statusCode = (int)httpStatusCode;

            if (statusCode.ToString().StartsWith("4") || statusCode.ToString().StartsWith("5"))
            {
                HttpContextWrapper.ChangeResponseHttpStatus(httpStatusCode);

                return new ServiceResponse<T>
                {
                    Success = false,
                    AdditionalMessages = additionalMessages.ToList(),
                    StatusCode = statusCode,
                    Data = data
                };
            }
            else
            {
                throw new ArgumentException("Invalid httpStatusCode, the status code passed must represent an error status code 4XX or 5XX");
            }
        }

        /// <summary>
        /// Create a ServiceResponse<T> with success state.
        /// Set the Response HttpStatus to 200(Ok).
        /// </summary>
        public static ServiceResponse<T> CreateOk(T data)
        {
            ServiceResponse response = ServiceResponse.CreateOk();
            return new ServiceResponse<T>
            {
                Success = response.Success,
                StatusCode = response.StatusCode,
                Data = data,
            };
        }

        /// <summary>
        /// Create a ServiceResponse with success state.
        /// Set the Response HttpStatus to received value.
        /// </summary>
        /// <param name="httpStatusCode"></param>
        /// <returns></returns>
        public static ServiceResponse<T> CreateOk(T data, HttpStatusCode httpStatusCode)
        {
            ServiceResponse baseResponse = ServiceResponse.CreateOk(httpStatusCode);
            return new ServiceResponse<T>
            {
                Success = baseResponse.Success,
                Data = data,
                StatusCode = (int)httpStatusCode,
            };
        }
    }

    /// <summary>
    /// The response contract to an service request.
    /// </summary>
    [DataContract(Name = "ServiceResponse")]
    [XmlRoot(ElementName = "ServiceResponse")]
    public class ServiceResponse
    {
        /// <summary>
        /// Default message for Unexpected internal error message = "A internal server error ocurred."
        /// </summary>
        public const string UNEXPECTED_INTERNAL_ERROR_MESSAGE = "A internal server error ocurred.";

        public ServiceResponse()
        {
            this.AdditionalMessages = new List<ServiceMessage>();
        }

        /// <summary>
        /// Service operation result.
        /// </summary>
        [DataMember]
        [XmlElement]
        public bool Success { get; set; }

        /// <summary>
        /// Collection of messages returned by the service operation.
        /// </summary>
        [DataMember]
        [XmlElement]
        public List<ServiceMessage> AdditionalMessages { get; set; }

        /// <summary>
        /// Resultant HttpStatus of service operation.
        /// </summary>
        [DataMember]
        [XmlElement]
        public int StatusCode { get; set; }

        /// <summary>
        /// Create a ServiceResponse with failure state, and a default internal error message on ValidationErrors property,
        /// logs the error and set the Response HttpStatus to 500(Internal Server Error).
        /// </summary>
        /// <param name="unexpectedException">Unexpected Exception</param>
        /// <returns>ServiceResponse with failure state</returns>
        public static ServiceResponse CreateError(Exception unexpectedException, bool logError = true)
        {
            // set the response http status to 500 - Internal Server Error.
            HttpContextWrapper.ChangeResponseHttpStatus(HttpStatusCode.InternalServerError);

            if (logError)
            {
                // log the error.
                //ServiceLogWritter.WriteError(unexpectedException);
            }

            return new ServiceResponse
            {
                Success = false,
                AdditionalMessages = GetUnexpectedInternalErrorValidationMessage().ToList(),
                StatusCode = (int)HttpStatusCode.InternalServerError,
            };
        }

        /// <summary>
        /// Create a ServiceResponse with failure state, and add the exception message to ValidationErrors property, and logs the error.
        /// </summary>
        /// <param name="serviceException">throwed ServiceException</param>
        /// <returns>ServiceResponse with failure state</returns>
        public static ServiceResponse CreateError(ServiceException serviceException, bool logError = true)
        {
            if (logError)
            {
                // log the error as warning.
                //ServiceLogWritter.WriteWarning(serviceException);
            }

            return new ServiceResponse
            {
                Success = false,
                AdditionalMessages = GetValidationMessage(serviceException).ToList(),
                StatusCode = (int)serviceException.HttpStatusCode,
            };
        }

        /// <summary>
        /// Create a ServiceResponse with success state.
        /// Set the Response HttpStatus to 200(Ok).
        /// </summary>
        public static ServiceResponse CreateOk()
        {
            return CreateOk(HttpStatusCode.OK);
        }

        public static ServiceResponse CreateOk(ICollection<string> additionalMessages)
        {
            return CreateOk(HttpStatusCode.OK, additionalMessages);
        }

        /// <summary>
        /// Create a ServiceResponse with success state.
        /// Set the Response HttpStatus to received value.
        /// </summary>
        /// <param name="httpStatusCode"></param>
        /// <returns></returns>
        public static ServiceResponse CreateOk(HttpStatusCode httpStatusCode, ICollection<string> additionalMessages = null)
        {
            HttpContextWrapper.ChangeResponseHttpStatus(httpStatusCode);
            ServiceResponse response = new ServiceResponse { Success = true, StatusCode = (int)httpStatusCode };

            if (additionalMessages == null)
            {
                return response;
            }

            foreach (var message in additionalMessages)
            {
                response.AdditionalMessages.Add(new ServiceMessage { Message = message });
            }

            return response;
        }

        /// <summary>
        /// Get the Exception message and add to returned collection.
        /// </summary>
        /// <param name="serviceException"></param>
        private static ICollection<ServiceMessage> GetValidationMessage(ServiceException serviceException)
        {
            ICollection<ServiceMessage> validationMessageCollection = new HashSet<ServiceMessage>();
            validationMessageCollection.Add(new ServiceMessage { Message = serviceException.Message });

            foreach (string serviceMessage in serviceException.AdditionalMessages)
            {
                validationMessageCollection.Add(new ServiceMessage() { Message = serviceMessage });
            }

            return validationMessageCollection;
        }

        /// <summary>
        /// Creates a collection with a unexpected error message added.
        /// </summary>
        private static ICollection<ServiceMessage> GetUnexpectedInternalErrorValidationMessage()
        {
            ICollection<ServiceMessage> serviceMessageCollection = new HashSet<ServiceMessage>();
            serviceMessageCollection.Add(new ServiceMessage { Message = UNEXPECTED_INTERNAL_ERROR_MESSAGE });

            return serviceMessageCollection;
        }
    }
}
