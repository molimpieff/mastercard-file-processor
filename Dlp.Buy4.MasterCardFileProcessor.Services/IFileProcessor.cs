﻿using Dlp.Buy4.MasterCardFileProcessor.DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Dlp.Buy4.MasterCardFileProcessor.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IFileProcessor
    {
        //[OperationContract]
        //[WebInvoke(Method = "GET", UriTemplate = "FileProcessor/ImportOutgoing?fileName={fileName}&filePath={filePath}")]
        //ServiceResponse ImportOutgoingFile(string fileName, string filePath);

        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "ImportOutgoing")]
        ServiceResponse ImportOutgoingFile(OutgoingImportRequest request);
    }


}
