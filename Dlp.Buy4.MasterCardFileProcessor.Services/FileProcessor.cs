﻿using Dlp.Buy4.MasterCardFileProcessor.DataContract;
using Dlp.Buy4.MasterCardFileProcessor.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dlp.Buy4.MasterCardFileProcessor.Commons;

namespace Dlp.Buy4.MasterCardFileProcessor.Services
{
    public class FileProcessor : IFileProcessor
    {
        OutgoingImportManager outgoingImportManager;

        public FileProcessor()
        {
            outgoingImportManager = new OutgoingImportManager();
        }

        //public ServiceResponse ImportOutgoingFile(string fileName, string filePath)
        //{
        //    return this.outgoingImportManager.Execute(fileName, filePath);
        //}

        public ServiceResponse ImportOutgoingFile(OutgoingImportRequest request)
        {
            try
            {
                return this.outgoingImportManager.Execute(request.FileName, request.FilePath);
                
            }
            catch (Exception e)
            {
                Log.WriteError(e.Message);
                return ServiceResponse.CreateError(e);
            }
            
        }

    }
}