﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dlp.Buy4.MasterCardFileProcessor.Repository.Clearing.Model
{
    public class MasterCardFinancialAddendum
    {
        public int MasterCardFinancialAddendumId { get; set; }

        public DateTime CreationDate { get; set; }

        public int ClearingFileId { get; set; }

        public string Mti { get; set; }

        public string FunctionCode { get; set; }

        public string AcquiringInstitutionIdCode { get; set; }

        public string FowardInstitutionIdCode { get; set; }

        public int? MessageNumber { get; set; }

        public string TransactionDestinationInsutitutionId { get; set; }

        public string TransactionOriginatorInstitutionId { get; set; }

        public string ReceivingInstitutionId { get; set; }

        public string TransactionDescription { get; set; }

        public string FreeFormDescription { get; set; }

        public string TypeOfInstallmentPayment { get; set; }

        public string SalesAmount { get; set; }

        public string InstallmentPaymentNumber { get; set; }

        public string InstallmentPaymentAmount { get; set; }

        public string AirportFeeAmount { get; set; }

        public string DownPaymentAmount { get; set; }

        public string NumberOfInstallments { get; set; }

        public int? MasterCardFirstPresentmentId { get; set; }

        public string TransactionDescriptionUsageCode { get; set; }

        public string TransactionDescriptionOccurrenceIndicator { get; set; }

        public string TransactionDescriptionIndustryRecordNumber { get; set; }

        public string TransactionDescriptionAssociatedFirstPresentment { get; set; }

        public string PdsData { get; set; }

    }

}
