﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dlp.Buy4.MasterCardFileProcessor.Repository.Clearing.Model
{
    public class MasterCardFirstPresentment
    {
        public int MasterCardFirstPresentmentId { get; set; }

        public DateTime CreationDate { get; set; }

        public int ClearingFileId { get; set; }

        public string Mti { get; set; }

        public string Pan { get; set; }

        public string Bin { get; set; }

        public string ProcessingCode { get; set; }

        public string AmountTransaction { get; set; }

        public string AmountReconciliation { get; set; }

        public string AmountCardholderBilling { get; set; }

        public string ConversionRateReconciliation { get; set; }

        public string ConversionRateCardholderBilling { get; set; }

        public string DateTimeLocalTransaction { get; set; }

        public string ExpirationDate { get; set; }

        public string PosData { get; set; }

        public string CardSequenceNumber { get; set; }

        public string FunctionCode { get; set; }

        public string MessageReasonCode { get; set; }

        public string Mcc { get; set; }

        public string AmountsOriginal { get; set; }

        public string AcquirerReferenceNumber { get; set; }

        public string AcquiringInstitutionIdCode { get; set; }

        public string FowardInstitutionIdCode { get; set; }

        public string RetrievalReferenceNumber { get; set; }

        public string ApprovalCode { get; set; }

        public string ServiceCode { get; set; }

        public string CardAcceptorTerminalId { get; set; }

        public string CardAcceptorId { get; set; }

        public string CardAcceptorNameLocation { get; set; }

        public string CurrencyCodeTransaction { get; set; }

        public string CurrencyCodeReconciliation { get; set; }

        public string CurrencyCodeCardholderBilling { get; set; }

        public string AmountsAdditional { get; set; }

        public string TransactionLifecycleId { get; set; }

        public int? MessageNumber { get; set; }

        public string TransactionDestinationInsutitutionId { get; set; }

        public string TransactionOriginatorInstitutionId { get; set; }

        public string ReceivingInstitutionId { get; set; }

        public string AmountCurrencyConversionAssessment { get; set; }

        public string TerminalType { get; set; }

        public string MessageReversalIndicator { get; set; }

        public string FileReversalIndicator { get; set; }

        public string Eci { get; set; }

        public string CurrencyExponents { get; set; }

        public string BusinessActivity { get; set; }

        public string SettlementIndicator { get; set; }

        public string PdsData { get; set; }

        public string DataRecord { get; set; }

        public string CardIssuerReferenceData { get; set; }

        public string ExternalAuthorizationId { get; set; }

        public int? IdConfirmedTransaction { get; set; }

        public string CardNumber { get; set; }

        public DateTime? AuthorizationDate { get; set; }

        public DateTime? ConfirmationDate { get; set; }

        public int? ClearingOperationStatus { get; set; }

        public string BrazilMerchantTaxId { get; set; }

    }

}
