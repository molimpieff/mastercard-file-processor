﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dlp.Buy4.MasterCardFileProcessor.Repository.Clearing.Model
{
    public class MasterCardFileHeader
    {
        public int MasterCardFileHeaderId { get; set; }

        public DateTime CreationDate { get; set; }

        public int ClearingFileId { get; set; }

        public string Mti { get; set; }

        public string FunctionCode { get; set; }

        public int? MessageNumber { get; set; }

        public string FileReversalIndicator { get; set; }

        public string FileId { get; set; }

        public string TransmissionId { get; set; }

        public string ProcessingMode { get; set; }

        public string OriginatingMessageFormat { get; set; }

    }
}
