﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dlp.Buy4.MasterCardFileProcessor.Repository.Clearing.Model
{
    public class MasterCardFileTrailer
    {
        public int MasterCardFileTrailerId { get; set; }

        public DateTime CreationDate { get; set; }

        public int ClearingFileId { get; set; }

        public string Mti { get; set; }

        public string FunctionCode { get; set; }

        public int? MessageNumber { get; set; }

        public string FileId { get; set; }

        public string OriginatingMessageFormat { get; set; }

        public string FileAmountChecksum { get; set; }

        public string FileMessageCounts { get; set; }

    }
}
