﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dlp.Buy4.MasterCardFileProcessor.Repository.Clearing.Model
{
    public sealed class ClearingFile
    {
        public int ClearingFileId { get; set; }

        public string FileName { get; set; }

        public string FilePath { get; set; }

        public string FileType = "Mastercard";

        public string FileDirection { get; set; }

        public DateTime ProcessingDate { get; set; }

        public int FileId { get; set; }

        public int FinancialTransactionsCount { get; set; }

        public int FinancialTransactionsAmount { get; set; }
    }
}
