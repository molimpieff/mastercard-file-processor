﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dlp.Buy4.MasterCardFileProcessor.Repository.Clearing.Model
{
    public class MasterCardFeeCollectionOther
    {
        public int MasterCardFeeCollectionOtherId { get; set; }

        public DateTime CreationDate { get; set; }

        public string Mti { get; set; }

        public string Pan { get; set; }

        public string ProcessingCode { get; set; }

        public string AmountTransaction { get; set; }

        public string AmountReconciliation { get; set; }

        public string ConversionRateReconciliation { get; set; }

        public string CardSequenceNumber { get; set; }

        public string FunctionCode { get; set; }

        public string MessageReasonCode { get; set; }

        public string MCC { get; set; }

        public string AmountsOriginal { get; set; }

        public string AcquirerReferenceData { get; set; }

        public string AcquiringInstitutionCode { get; set; }

        public string ForwardingInstitutionIdCode { get; set; }

        public string ApprovalCode { get; set; }

        public string CardAcceptorTerminalId { get; set; }

        public string CardAcceptorIdCode { get; set; }

        public string CardAcceptorNameLocation { get; set; }

        public string GCMSProductIdentifier { get; set; }

        public string LicensedProductIdentifier { get; set; }

        public string MessageReversalIndicator { get; set; }

        public string AmountTax { get; set; }

        public string FeeCollectionControlNumber { get; set; }

        public string CurrencyExponents { get; set; }

        public string CurrencyCodesAmountsOriginal { get; set; }

        public string BusinessActivity { get; set; }

        public string SettlementData { get; set; }

        public string SettlementIndicator { get; set; }

        public string CardAcceptorURL { get; set; }

        public string OriginatingMessageFormat { get; set; }

        public string DocumentationIndicator { get; set; }

        public string InitialPresentmentFeeCollectionData { get; set; }

        public string FirstChargebackFeeCollectionReturnData { get; set; }

        public string SecondPresentmentFeeCollectionResubmissionData { get; set; }

        public string MemberReconciliationIndicatorOne { get; set; }

        public string TestCaseTraceabilityIdentifiers { get; set; }

        public string MemberToMemberProprietaryData { get; set; }

        public string CurrencyCodeTransaction { get; set; }

        public string CurrencyCodeReconciliation { get; set; }

        public string TransactionLifeCycleID { get; set; }

        public int MessageNumber { get; set; }

        public string DataRecord { get; set; }

        public string DateAction { get; set; }

        public string TransactionDestinationInstitutionIdCode { get; set; }

        public string TransactionOriginatorInstitutionIdCode { get; set; }

        public string ReceivingInstitutionIdCode { get; set; }

        public int? ClearingFileId { get; set; }

        public DateTime? LastModifiedDate { get; set; }

    }

}
