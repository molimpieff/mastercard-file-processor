﻿using Dlp.Buy4.MasterCardFileProcessor.Commons.DataObject;
using Dlp.Buy4.MasterCardFileProcessor.Repository.Clearing.Model;
using Dlp.Connectors;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dlp.Buy4.MasterCardFileProcessor.Repository.Clearing
{
    /// <summary>
    /// buy4_clearing.dbo.MasterCardFirstPresentment table repository
    /// </summary>
    public class MasterCardFirstPresentmentRepository
    {
        DatabaseConnector dbConnection;

        public MasterCardFirstPresentmentRepository()
        {
            this.dbConnection = new DatabaseConnector(ConfigurationManager.ConnectionStrings["Buy4_clearing"].ConnectionString, 120);
        }

        /// <summary>
        /// method responsible to insert the PresentmentCollection on the database
        /// </summary>
        /// <param name="presentmentCollection">collection of Presentment objects</param>
        /// <param name="clearingFileId">clearingFile's id that will associate the Presentment with the clearingFile</param>
        public void BulkInsertPresentment(IEnumerable<Presentment> presentmentCollection, int clearingFileId)
        {
            List<MasterCardFirstPresentment> mfpList = new List<MasterCardFirstPresentment>();

            foreach(Presentment presentment in presentmentCollection)
            {
                MasterCardFirstPresentment mfp = new MasterCardFirstPresentment();
                mfp.ClearingFileId = clearingFileId;
                mfp.CreationDate = DateTime.Now;
                mfp.AcquirerReferenceNumber = presentment.AcquirerReferenceData;
                mfp.AcquiringInstitutionIdCode = presentment.AcquiringInstitutionIdCode;
                mfp.AmountCardholderBilling = presentment.AmountCardholderBilling;
                mfp.AmountCurrencyConversionAssessment = presentment.AmountCurrencyConversionAssessment;
                mfp.AmountReconciliation = presentment.AmountReconciliation;
                mfp.AmountsAdditional = presentment.AmountsAdditional;
                mfp.AmountsOriginal = presentment.AmountsOriginal;
                mfp.AmountTransaction = presentment.AmountTransaction;
                mfp.ApprovalCode = presentment.ApprovalCode;
                mfp.Bin = presentment.Bin;
                mfp.BrazilMerchantTaxId = presentment.BrazilMerchantTaxId;
                mfp.BusinessActivity = presentment.BusinessActivity;
                mfp.CardAcceptorId =presentment.CardAcceptorId;
                mfp.CardAcceptorNameLocation = presentment.CardAcceptorNameLocation;
                mfp.CardAcceptorTerminalId = presentment.CardAcceptorTerminalId;
                mfp.CardIssuerReferenceData = presentment.CardIssuerReferenceData;
                mfp.CardSequenceNumber = presentment.CardSequenceNumber;
                mfp.ConversionRateCardholderBilling = presentment.ConversionRateCardholderBilling;
                mfp.ConversionRateReconciliation = presentment.ConversionRateReconciliation;
                mfp.CurrencyCodeCardholderBilling = presentment.CurrencyCodeCardholderBilling;
                mfp.CurrencyCodeReconciliation = presentment.CurrencyCodeReconciliation;
                mfp.CurrencyCodeTransaction = presentment.CurrencyCodeTransaction;
                mfp.CurrencyExponents = presentment.CurrencyExponents;
                mfp.DataRecord = presentment.DataRecord;
                mfp.DateTimeLocalTransaction = presentment.DateTimeLocalTransaction;
                mfp.Eci = presentment.Eci;
                mfp.ExpirationDate = presentment.ExpirationDate;
                mfp.FileReversalIndicator = presentment.FileReversalIndicator;
                mfp.FowardInstitutionIdCode = presentment.FowardInstitutionIdCode;
                mfp.FunctionCode = presentment.FunctionCode;
                mfp.Mcc = presentment.Mcc;
                mfp.MessageNumber = presentment.MessageNumber;
                mfp.MessageReasonCode = presentment.MessageReasonCode;
                mfp.MessageReversalIndicator = presentment.MessageReversalIndicator;
                mfp.Mti = presentment.Mti;
                mfp.Pan = presentment.Pan;
                mfp.PdsData = presentment.PdsData;
                mfp.PosData = presentment.PosData;
                mfp.ProcessingCode = presentment.ProcessingCode;
                mfp.ReceivingInstitutionId = presentment.ReceivingInstitutionId;
                mfp.RetrievalReferenceNumber = presentment.RetrievalReferenceNumber;
                mfp.ServiceCode = presentment.ServiceCode;
                mfp.SettlementIndicator = presentment.SettlementIndicator;
                mfp.TerminalType = presentment.TerminalType;
                mfp.TransactionDestinationInsutitutionId = presentment.TransactionDestinationInsutitutionId;
                mfp.TransactionLifecycleId = presentment.TransactionLifecycleId;
                mfp.TransactionOriginatorInstitutionId = presentment.TransactionOriginatorInstitutionId;

                mfpList.Add(mfp);
            }

            dbConnection.BulkInsert("MasterCardFirstPresentment", mfpList, SqlBulkCopyOptions.Default);
        }

    }
}
