﻿using Dlp.Buy4.MasterCardFileProcessor.Commons.DataObject;
using Dlp.Buy4.MasterCardFileProcessor.Repository.Clearing.Model;
using Dlp.Connectors;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dlp.Buy4.MasterCardFileProcessor.Repository.Clearing
{
    /// <summary>
    /// buy4_clearing.dbo.MasterCardFeeCollection table repository
    /// </summary>
    public class MasterCardFeeCollectionRepository
    {
         DatabaseConnector dbConnection;

        public MasterCardFeeCollectionRepository()
        {
            this.dbConnection = new DatabaseConnector(ConfigurationManager.ConnectionStrings["Buy4_clearing"].ConnectionString, 120);
        }

        /// <summary>
        /// method responsible to insert the FeeCollectionCollection on the database
        /// </summary>
        /// <param name="presentmentCollection">collection of FeeCollection objects</param>
        /// <param name="clearingFileId">clearingFile's id that will associate the FeeCollection with the clearingFile</param>
        public void BulkInsertFeeCollection(IEnumerable<FeeCollection> feeCollectionCollection, int clearingFileId)
        {
            List<MasterCardFeeCollectionOther> mfcList = new List<MasterCardFeeCollectionOther>();

            foreach(FeeCollection feeCollection in feeCollectionCollection)
            {
                MasterCardFeeCollectionOther mfc = new MasterCardFeeCollectionOther();

                mfc.AcquirerReferenceData = feeCollection.AcquirerReferenceData;
                mfc.AcquiringInstitutionCode = feeCollection.AcquiringInstitutionIdentificationCode;
                mfc.AmountReconciliation = feeCollection.AmountReconciliation;
                mfc.AmountsOriginal = feeCollection.AmountsOriginal;
                mfc.AmountTax = feeCollection.AmountTax;
                mfc.AmountTransaction = feeCollection.AmountTransaction;
                mfc.ApprovalCode = feeCollection.ApprovalCode;
                mfc.BusinessActivity = feeCollection.BusinessActivity;
                mfc.CardAcceptorIdCode = feeCollection.CardAcceptorIdCode;
                mfc.CardAcceptorNameLocation = feeCollection.CardAcceptorNameLocation;
                mfc.CardAcceptorTerminalId = feeCollection.CardAcceptorTerminalId;
                mfc.CardAcceptorURL = feeCollection.CardAcceptorURL;
                mfc.CardSequenceNumber = feeCollection.CardSequenceNumber;
                mfc.ClearingFileId = clearingFileId;
                mfc.ConversionRateReconciliation = feeCollection.ConversionRateReconciliation;
                mfc.CreationDate = DateTime.Now;
                mfc.CurrencyCodeReconciliation = feeCollection.CurrencyCodeReconciliation;
                mfc.CurrencyCodesAmountsOriginal = feeCollection.CurrencyCodesAmountsOriginal;
                mfc.CurrencyCodeTransaction = feeCollection.CurrencyCodeTransaction;
                mfc.CurrencyExponents = feeCollection.CurrencyExponents;
                mfc.DataRecord = feeCollection.DataRecord;
                mfc.DateAction = feeCollection.DateAction;
                mfc.DocumentationIndicator = feeCollection.DocumentationIndicator;
                mfc.FeeCollectionControlNumber = feeCollection.FeeCollectionControlNumber;
                mfc.FirstChargebackFeeCollectionReturnData = feeCollection.FirstChargebackFeeCollectionReturnData;
                mfc.ForwardingInstitutionIdCode = feeCollection.ForwardingInstitutionIdCode;
                mfc.FunctionCode = feeCollection.FunctionCode;
                mfc.GCMSProductIdentifier = feeCollection.GCMSProductIdentifier;
                mfc.InitialPresentmentFeeCollectionData = feeCollection.InitialPresentmentFeeCollectionData;
                mfc.LicensedProductIdentifier = feeCollection.LicensedProductIdentifier;
                mfc.MCC = feeCollection.MCC;
                mfc.MemberReconciliationIndicatorOne = feeCollection.MemberReconciliationIndicatorOne;
                mfc.MemberToMemberProprietaryData = feeCollection.MemberToMemberProprietaryData;
                mfc.MessageNumber = feeCollection.MessageNumber;
                mfc.MessageReasonCode = feeCollection.MessageReasonCode;
                mfc.MessageReversalIndicator = feeCollection.MessageReversalIndicator;
                mfc.Mti = feeCollection.Mti;
                mfc.OriginatingMessageFormat = feeCollection.OriginatingMessageFormat;
                mfc.Pan = feeCollection.Pan;
                mfc.ProcessingCode = feeCollection.ProcessingCode;
                mfc.ReceivingInstitutionIdCode = feeCollection.ReceivingInstitutionIdCode;
                mfc.SecondPresentmentFeeCollectionResubmissionData = feeCollection.SecondPresentmentFeeCollectionResubmissionData;
                mfc.SettlementData = feeCollection.SettlementData;
                mfc.SettlementIndicator = feeCollection.SettlementIndicator;
                mfc.TestCaseTraceabilityIdentifiers = feeCollection.TestCaseTraceabilityIdentifiers;
                mfc.TransactionDestinationInstitutionIdCode = feeCollection.TransactionDestinationInstitutionIdCode;
                mfc.TransactionLifeCycleID = feeCollection.TransactionLifeCycleId;
                mfc.TransactionOriginatorInstitutionIdCode = feeCollection.TransactionOriginatorInstitutionIdCode;

                mfcList.Add(mfc);
            }

            dbConnection.BulkInsert("MasterCardFeeCollectionOther", mfcList, SqlBulkCopyOptions.Default);
        }
    }
}
