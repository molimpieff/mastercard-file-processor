﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dlp.Buy4.MasterCardFileProcessor.Commons.DataObject;
using Dlp.Buy4.MasterCardFileProcessor.Repository.Clearing.Model;
using Dlp.Connectors;
using System.Configuration;

namespace Dlp.Buy4.MasterCardFileProcessor.Repository.Clearing
{
    /// <summary>
    /// Buy4_Clearing.dbo.MasterCardFileTrailer table repository
    /// </summary>
    public class MasterCardFileTrailerRepository
    {
        public DatabaseConnector dbConnection;

        public MasterCardFileTrailerRepository()
        {
            this.dbConnection = new DatabaseConnector(ConfigurationManager.ConnectionStrings["Buy4_clearing"].ConnectionString);
        }

        /// <summary>
        /// method responsible for creating the MasterCardFileTrailer on the database
        /// </summary>
        /// <param name="fileTrailer">OutgoingFileTrailer object</param>
        /// <param name="clearingFileId">clearingFile's id that will associate with the fileTrailer</param>
        public void Create(OutgoingFileTrailer fileTrailer, int clearingFileId)
        {
            #region sql query
            string sqlQuery = @"INSERT INTO [dbo].[MasterCardFileTrailer]
           ([ClearingFileId]
           ,[Mti]
           ,[FunctionCode]
           ,[MessageNumber]
           ,[FileId]
           ,[OriginatingMessageFormat]
           ,[FileAmountChecksum]
           ,[FileMessageCounts])
    OUTPUT INSERTED.MasterCardFileTrailerId
     VALUES
           (@ClearingFileId, 
           @Mti, 
           @FunctionCode, 
           @MessageNumber, 
           @FileId, 
           @OriginatingMessageFormat, 
           @FileAmountChecksum,
           @FileMessageCounts);";
            #endregion

            int fileTrailerId = this.dbConnection.ExecuteScalar<int>(sqlQuery, new
                {
                    ClearingFileId = clearingFileId,
                    Mti = fileTrailer.Mti,
                    FunctionCode = fileTrailer.FunctionCode,
                    MessageNumber = fileTrailer.MessageNumber,
                    FileId = fileTrailer.FileId,
                    OriginatingMessageFormat = fileTrailer.OriginatingMessageFormat,
                    FileAmountChecksum = fileTrailer.FileAmountChecksum,
                    FileMessageCounts = fileTrailer.FileMessageCounts
                });
        }
    }
}
