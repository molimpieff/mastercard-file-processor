﻿using Dlp.Buy4.MasterCardFileProcessor.Commons.DataObject;
using Dlp.Connectors;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dlp.Buy4.MasterCardFileProcessor.Repository.Clearing
{
    /// <summary>
    /// Buy4_Clearing.dbo.MasterCardFileHeader table repository
    /// </summary>
    public class MasterCardFileHeaderRepository
    {
        DatabaseConnector dbConnection;

        public MasterCardFileHeaderRepository()
        {
            this.dbConnection = new DatabaseConnector(ConfigurationManager.ConnectionStrings["Buy4_clearing"].ConnectionString);
        }

        /// <summary>
        /// method responsible for creating the MasterCardFileHeader on the database
        /// </summary>
        /// <param name="fileHeader">OutgoingFileHeader object</param>
        /// <param name="clearingFileId">clearingFile's id that will associate with the fileHeader</param>
        public void Create(OutgoingFileHeader fileHeader, int clearingFileId)
        {
            #region sql query
            string sqlQuery = @"INSERT INTO [dbo].[MasterCardFileHeader]
           ([ClearingFileId]
           ,[Mti]
           ,[FunctionCode]
           ,[MessageNumber]
           ,[FileReversalIndicator]
           ,[FileId]
           ,[TransmissionId]
           ,[ProcessingMode]
           ,[OriginatingMessageFormat])
     VALUES
           (@ClearingFileId,
           @Mti,
           @FunctionCode, 
           @MessageNumber,
           @FileReversalIndicator, 
           @FileId, 
           @TransmissionId, 
           @ProcessingMode, 
           @OriginatingMessageFormat)";
            #endregion

            int fileHeaderId = dbConnection.ExecuteScalar<int>(sqlQuery, new
                {
                    ClearingFileId = clearingFileId,
                    Mti = fileHeader.Mti,
                    FunctionCode = fileHeader.FunctionCode,
                    MessageNumber = fileHeader.MessageNumber,
                    FileReversalIndicator = fileHeader.FileReversalIndicator,
                    FileId = fileHeader.FileId,
                    TransmissionId = fileHeader.TransmissionId,
                    ProcessingMode = fileHeader.ProcessingMode,
                    OriginatingMessageFormat = fileHeader.OriginatingMessageFormat
                });

        }
    }
}
