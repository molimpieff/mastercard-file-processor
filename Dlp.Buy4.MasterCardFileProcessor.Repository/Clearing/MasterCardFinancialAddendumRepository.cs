﻿using Dlp.Buy4.MasterCardFileProcessor.Commons.DataObject;
using Dlp.Buy4.MasterCardFileProcessor.Repository.Clearing.Model;
using Dlp.Connectors;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dlp.Buy4.MasterCardFileProcessor.Repository.Clearing
{
    /// <summary>
    /// Buy4_Clearing.dbo.MasterCardFinancialAddendum table repository
    /// </summary>
    public class MasterCardFinancialAddendumRepository
    {
        DatabaseConnector dbConnection;

        public MasterCardFinancialAddendumRepository()
        {
            this.dbConnection = new DatabaseConnector(ConfigurationManager.ConnectionStrings["Buy4_clearing"].ConnectionString, 120);
        }

        /// <summary>
        /// method responsible to insert the finnancialAddendumCollection on the database
        /// </summary>
        /// <param name="financialAddendumCollection">collection of financialAddendum objects</param>
        /// <param name="clearingFileId">clearingFile's id that will associate the financialAddendum with the clearingFile</param>
        public void BulkInsertFinancialAddendum(IEnumerable<FinancialAddendum>financialAddendumCollection, int clearingFileId)
        {
            List<MasterCardFinancialAddendum> mfaList = new List<MasterCardFinancialAddendum>();

            foreach(FinancialAddendum financialAddendum in financialAddendumCollection)
            {
                MasterCardFinancialAddendum mfa = new MasterCardFinancialAddendum();
                mfa.CreationDate = DateTime.Now;
                mfa.ClearingFileId = clearingFileId;
                mfa.Mti = financialAddendum.Mti;
                mfa.FunctionCode = financialAddendum.FunctionCode;
                mfa.AcquiringInstitutionIdCode = financialAddendum.AcquiringInstitutionIdCode;
                mfa.FowardInstitutionIdCode = financialAddendum.FowardInstutionIdCode;
                mfa.MessageNumber = financialAddendum.MessageNumber;
                mfa.TransactionDestinationInsutitutionId = financialAddendum.TransactionDestinationInsutitutionId;
                mfa.TransactionOriginatorInstitutionId = financialAddendum.TransactionOriginatorInstitutionId;
                mfa.ReceivingInstitutionId = financialAddendum.ReceivingInstitutionId;
                mfa.TransactionDescription = financialAddendum.TransactionDescription;
                mfa.FreeFormDescription = financialAddendum.FreeFormDescription;
                mfa.TypeOfInstallmentPayment = financialAddendum.TypeOfInstallmentPayment;
                mfa.SalesAmount = financialAddendum.SalesAmount;
                mfa.InstallmentPaymentNumber = financialAddendum.InstallmentPaymentNumber;
                mfa.InstallmentPaymentAmount = financialAddendum.InstallmentPaymentAmount;
                mfa.AirportFeeAmount = financialAddendum.AirportFeeAmount;
                mfa.DownPaymentAmount = financialAddendum.DownPaymentAmount;
                mfa.NumberOfInstallments = financialAddendum.NumberOfInstallments;
                //mfa.MasterCardFirstPresentmentId = masterCardFirstPresentmentId;
                mfa.TransactionDescriptionUsageCode = financialAddendum.TransactionDescriptionUsageCode;
                mfa.TransactionDescriptionOccurrenceIndicator = financialAddendum.TransactionDescriptionOccurrenceIndicator;
                mfa.TransactionDescriptionIndustryRecordNumber = financialAddendum.TransactionDescriptionIndustryRecordNumber;
                mfa.TransactionDescriptionAssociatedFirstPresentment = financialAddendum.TransactionDescriptionAssociatedFirstPresentment;
                mfa.PdsData = financialAddendum.PdsData;

                mfaList.Add(mfa);
            }

            dbConnection.BulkInsert("MasterCardFinancialAddendum", mfaList, System.Data.SqlClient.SqlBulkCopyOptions.Default);
        }
    }
}
