﻿using Dlp.Connectors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Dlp.Buy4.MasterCardFileProcessor.Repository.Clearing.Model;
using System.Data.SqlClient;
using Dlp.Buy4.MasterCardFileProcessor.Commons.DataObject;

namespace Dlp.Buy4.MasterCardFileProcessor.Repository.Clearing
{
    /// <summary>
    /// Class responsible for all methods that create, retrieve and update ClearingFile table.
    /// </summary>
    public sealed class ClearingFileRepository
    {

        DatabaseConnector dbConnection;

        /// <summary>
        /// Instances database connection.
        /// </summary>
        public ClearingFileRepository()
        {
            this.dbConnection = new DatabaseConnector(ConfigurationManager.ConnectionStrings["Buy4_clearing"].ConnectionString);
        }


        public bool ClearingFileExists(string fileName)
        {
            // TODO: Mapear objeto

            string sqlQuery = @"select count(1) from dbo.ClearingFile (NOLOCK) where FileName = @FileName";

            int numberOfClearingFiles = dbConnection.ExecuteScalar<int>(sqlQuery, new
            {
                FileName = fileName
            });


            if (numberOfClearingFiles > 0)
                return true;
            else
                return false;
        }
        
        /// <summary>
        /// Create the clearingFile on the datavase
        /// </summary>
        /// <param name="file">OutgoingFile Object</param>
        /// <returns>ClearingFileId inserted</returns>
        public int Create(OutgoingFile file)
        {
            string sqlQuery = @"INSERT INTO [dbo].[ClearingFile]
           ([FileName]
           ,[FilePath]
           ,[FileType]
           ,[FileDirection]
           ,[FileId]
           ,[FinancialTransactionsCount]
           ,[FinancialTransactionsAmount])
    OUTPUT INSERTED.ClearingFileId 
     VALUES
           (@FileName, 
           @FilePath, 
           @FileType, 
           @FileDirection, 
           @FileId, 
           @FinancialTransactionsCount, 
           @FinancialTransactionsAmount)";

            int clearingFileId = dbConnection.ExecuteScalar<int>(sqlQuery, new
                {
                    FileName = file.FileName,
                    FilePath = file.FilePath,
                    FileType = "Mastercard",
                    FileDirection = "Outgoing",
                    FileId = file.FileId,
                    FinancialTransactionsCount = file.FinancialTransactionsCount,
                    FinancialTransactionsAmount = file.FinancialTransactionsAmount
                });

            return clearingFileId;
        }
    }
}
